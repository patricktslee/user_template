# Load the Rails application.
require File.expand_path('../application', __FILE__)

VALID_LANG = ["zh-HK", "en"]

# Initialize the Rails application.
Rails.application.initialize!
